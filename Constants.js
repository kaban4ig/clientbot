/**
 * Created by Никита on 03.01.2018.
 */
//TODO add tests

constants = {
    globalTest: false,
    strategies: {

        strategyBuyStates: {
            readyForBuy: "readyForBuy",
            waitBuyOrCancel: "waitBuyOrCancel",
            buyingOrCanceling: "buyingOrCanceling",
            checkingOrder: "checkingOrder",
            bought: "bought",
            buyIsCanceled: "buyIsCanceled"
        },

        strategyBuyCommands: {
            cancelByTime: "cancelByTime",
            buy: "buy",
            buyAccepted: "buyAccepted",
            buyRejected: "buyRejected",
            checkOrder: "checkOrder",
            checkOrderIsDone: "checkOrderIsDone",
            wasCanceledByMe: "wasCanceledByMe",
            buyIsDone: "buyIsDone"
        },

        strategySellStates: {
            readyForSell: "readyForSell",
            waitSellOrCancel: "waitSellOrCancel",
            sellingOrCanceling: "sellingOrCanceling",
            checkingOrder: "checkingOrder",
            sold: "sold"
        },

        strategySellCommands: {
            sell: "sells",
            sellAccepted: "sellAccepted",
            sellRejected: "sellRejected",
            checkOrder: "checkOrder",
            checkOrderIsDone: "checkOrderIsDone",
            wasCanceledByMe: "wasCanceledByMe",
            sellIsDone: "sellIsDone"
        }



    },
    defaultSettings: {
        timeForBuyMinutes: 60,
        amountOfPurchase: 0.0006,
        sellReopenTimeSeconds: 45,
        buyReopenTimeSeconds: 45,
        addBuy2Percent: 3,
        maxTakeProfitCount: 6
    }
}

module.exports = constants;
/**
 * Created by Никита on 03.01.2018.
 */

var http = require('http');
var port = process.env.PORT || 3000;

var server = http.createServer(function(request, response) {
    console.log((new Date()) + ' Received request for ' + request.url);
    response.writeHead(404);
    response.end();
});
server.listen(port, function() {
    console.log((new Date()) + ' Client is listening on port ' + port);
});

var Acceptor = require("./Communications/Acceptor");
var Parser = require("./Communications/Parser");
var SignalController = require("./MarketEnvironment/SignalController");
var BittrexController = require("./BittrexWrap/BittrexController");

//var acceptor = new Acceptor("ws://jackserver.us-east-2.elasticbeanstalk.com?keyClient=d41d8cd98f00b204e9800998ecf8427e");
var acceptor = new Acceptor("ws://localhost:8080?keyClient=d41d8cd98f00b204e9800998ecf8427e");
var parser = new Parser();
var bittrexController = new BittrexController();
bittrexController.start();
var signalController = new SignalController(bittrexController);
signalController.start();
acceptor.conntect(check);

function check(info) {
    let parsedInfo = parser.getInfoFromJSON(info);
    let result = signalController.addSignal(parsedInfo);
    if (result.isError) {
        console.log(result.errorMessage);
    } else {
        console.log("signal added");
    }

}
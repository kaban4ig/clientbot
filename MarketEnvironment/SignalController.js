/**
 * Created by Никита on 03.01.2018.
 */
//TODO add tests

var Signal = require("./Signal");
var Validator = require("./Validators/Validator");


class SignalController {
    constructor(bittrexController) {
        this._signals = [];
        this.bittrexController = bittrexController;
        this.isActive = false;
        this._updateSignals();
    }

    _updateSignals() {
        setTimeout(() => {
            this.bittrexController.updateMarkets(this._signals.map(s => s.marketName));
            this._updateSignals();
        }, 20000);
    }

    addSignal(infoObj) {


            let result ={
                errorMessage: "",
                isError: false
            }

            infoObj.isValidInfo =  Validator.checkInfo(infoObj);

            if (infoObj.isValidInfo) {
                let signal = new Signal(infoObj, this.bittrexController, infoObj.settings);

                // we work only with one market exempler
                let isSignalAlreadyUsed = this._signals.some(s => s.marketName === signal.marketName);

                if (!isSignalAlreadyUsed && !signal.isError) {
                    this._signals.push(signal);

                } else {
                    result.isError = true;
                    if (isSignalAlreadyUsed) result.errorMessage += ";signal already used";
                    if (signal.isError) result.errorMessage += signal.errorMessage
                }
            } else {
                result.isError = true;
                result.errorMessage += ";Not valid info" + infoObj;
            }
            return result;

    }

    start() {
        this.isActive = true;
        this.do();
    }

    stop() {
        this.isActive = false;
    }

    do() {
        if (this._signals && this._signals.length > 0) {
            this._signals = this._signals.filter(s => s.isActive);
            this._signals.forEach(s => s.do());
        }
        if (this.isActive) {
            setTimeout(() => {
                this.do();
            }, 0);
        }
    }
}

module.exports = SignalController;


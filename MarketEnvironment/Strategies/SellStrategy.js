/**
 * Created by Никита on 03.01.2018.
 */
//TODO add tests

var constants = require("../../Constants");
var helpers = require("../../Helpers");
var SellStrategyState = require("./StrategyStates/SellStrategyState");

class SellStrategy {
    constructor(signalObj, bittrexController, settings, strategyName) {
        this.strategynName = strategyName;
        this._settings = settings || constants.defaultSettings;
        this.bittrexController = bittrexController;

        this.takeProfit = signalObj.takeProfit;
        this.stopLoss = signalObj.stopLoss;
        this.marketQuantity = signalObj.marketQuantity;

        this.marketName = signalObj.marketName;

        this._strategyStates = constants.strategies.strategySellStates;
        this._strategyCommands = constants.strategies.strategySellCommands;

        this.sellStrategyState = new SellStrategyState();

        this._sellInfo = {
            uuid: null,
            lastStartSellTime: null
        }
    }

    do() {

        if (this.sellStrategyState.currentState === this._strategyStates.readyForSell) {
            //console.log("try sell");
            this._trySell();
        }

        if (this.sellStrategyState.currentState === this._strategyStates.sellingOrCanceling) {
            //console.log("try check order");
            this._tryCheckOrder();
        }


    }

    _trySell() {
        let marketInfo = this.bittrexController.getMarketInfo(this.marketName);
        if (marketInfo) {
            let ask = marketInfo.result.Ask;

            if((ask <= this.stopLoss || ask >= this.takeProfit) && this.sellStrategyState.tryChangeState(this._strategyCommands.sell)) {
                //console.log("sell limit");
                this.bittrexController.sellLimit(this.marketName, this.marketQuantity, ask).then(
                    response => {
                        /*console.log("Was trying sell");
                        console.log("Strategy name: " + this.strategynName);
                        console.log("market name: " + this.marketName);
                        console.log("quantity: " + this.marketQuantity);
                        console.log("sell ask is: " + ask);
                        console.log("takeProfit is: " + this.takeProfit);*/
                        if (response.success === true) {
                            this._sellInfo.uuid = response.result.uuid;
                            this._sellInfo.lastStartSellTime = Date.now();
                            this.sellStrategyState.tryChangeState(this._strategyCommands.sellAccepted);
                            //console.log("Sell in progress now!, uuid:" + this._sellInfo.uuid);
                        } else {
                            //console.log("sell order is rejected!!!");
                            //console.log(response.message);
                            this.sellStrategyState.tryChangeState(this._strategyCommands.sellRejected);
                        }
                    },
                    error => {
                        this.sellStrategyState.tryChangeState(this._strategyCommands.sellRejected);
                        //console.log("sell order is rejected!!!");
                        //console.log(response.message);
                    });
            }
        } else {
            //console.log("no market info");
        }
    }

    _tryCheckOrder() {
        if (this.sellStrategyState.tryChangeState(this._strategyCommands.checkOrder)) {
            this.bittrexController.getOrder(this._sellInfo.uuid).then(
                response => {
                    if (response.success) {
                        this.sellStrategyState.tryChangeState(this._strategyCommands.checkOrderIsDone);
                        if (!response.result.IsOpen) {
                            if (response.result.CancelInitiated) {
                                console.log("we canceled");
                                console.log("Uuid: " + response.result.OrderUuid);
                                this.sellStrategyState.tryChangeState(this._strategyCommands.wasCanceledByMe);
                            } else  {
                                console.log("sell is sold");
                                console.log("Uuid: " + response.result.OrderUuid);
                                this.sellStrategyState.tryChangeState(this._strategyCommands.sellIsDone);
                            }
                        } else if (response.result.IsOpen) {
                            //else we already change state in checkOrderIsDone
                            //it is not important what bittrex will answered, we just try closed order if needed
                            if (helpers.convertMillisecondsToSeconds(Date.now() - this._sellInfo.lastStartSellTime) > this._settings.sellReopenTimeSeconds) {
                                this._tryCloseOrder(this._sellInfo.uuid);
                            }
                        }
                    } else {
                        this.sellStrategyState.tryChangeState(this._strategyCommands.checkOrderIsDone);
                    }
                },
                error => {
                    this.sellStrategyState.tryChangeState(this._strategyCommands.checkOrderIsDone);
                });
        }
    }

    _tryCloseOrder(uuid) {
        this.bittrexController.cancelOrder(uuid).then(
            response => {
                console.log("close result:");
                console.log(response);
            },
            error => {
                console.log("close result:");
                console.log(error);
            });
    }

}

module.exports = SellStrategy;


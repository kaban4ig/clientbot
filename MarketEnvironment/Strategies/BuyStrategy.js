/**
 * Created by Никита on 03.01.2018.
 */
//TODO add tests

var constants = require("../../Constants");
var helpers = require("../../Helpers");
var BuyStrategyState = require("./StrategyStates/BuyStrategyState");

class BuyStrategy {
    constructor(signalObj, bittrexController, settings) {
        this._settings = settings || constants.defaultSettings;
        this.bittrexController = bittrexController;

        this._amountOfPurchase = this._settings.amountOfPurchase;

        this.marketName = signalObj.marketName;
        this._minBuy = signalObj.buy[0];
        this._maxBuy = signalObj.buy[signalObj.buy.length-1];
        this._startSignalTime = Date.now();
        this._strategyStates = constants.strategies.strategyBuyStates;
        this._strategyCommands = constants.strategies.strategyBuyCommands;

        this.buyStrategyState = new BuyStrategyState();

        this._buyInfo = {
            uuid: null,
            lastStartBuyTime: null
        }
        this._uuid = null;
    }

    do() {
        if (!this._isActualSignalForBuy()) {
            //console.log("check actual");
            this.buyStrategyState.tryChangeState(this._strategyCommands.cancelByTime);
        }

        if (this.buyStrategyState.currentState === this._strategyStates.readyForBuy) {
            //console.log("try buy");
            this._tryBuy();
        }

        if (this.buyStrategyState.currentState === this._strategyStates.buyingOrCanceling) {
            //console.log("try check order");
            this._tryCheckOrder();
        }


    }

    _tryBuy() {
        let marketInfo = this.bittrexController.getMarketInfo(this.marketName);
        if (marketInfo) {
            let bid = marketInfo.result.Bid;
            if(bid < (this._maxBuy + helpers.getNumberFromPercent(this._maxBuy, this._settings.addBuy2Percent)) && this.buyStrategyState.tryChangeState(this._strategyCommands.buy)) {
                //console.log("buy limit");
                this.bittrexController.buyLimit(this.marketName, this._amountOfPurchase/bid, bid).then(
                    response => {
                        console.log("was trying buy");
                        console.log("market name: " + this.marketName);
                        console.log("quantity: " + this._amountOfPurchase/bid);
                        console.log("buy bid is: " + bid);
                        if (response.success === true) {
                            this._buyInfo.uuid = response.result.uuid;
                            this._buyInfo.lastStartBuyTime = Date.now();
                            this.buyStrategyState.tryChangeState(this._strategyCommands.buyAccepted);
                            console.log("Buy in progress now!, uuid:" + this._buyInfo.uuid);
                        } else {
                            console.log("buy order is rejected");
                            console.log(response.message);
                            this.buyStrategyState.tryChangeState(this._strategyCommands.buyRejected);
                        }
                    },
                    error => {
                        this.buyStrategyState.tryChangeState(this._strategyCommands.buyRejected);
                        console.log("buy order is rejected");
                        console.log(response.message);
                    });
            }
        } else {
            //console.log("no market info");
        }
    }

    _tryCheckOrder() {
        if (this.buyStrategyState.tryChangeState(this._strategyCommands.checkOrder)) {
            this.bittrexController.getOrder(this._buyInfo.uuid).then(
                response => {
                    if (response.success) {
                        this.buyStrategyState.tryChangeState(this._strategyCommands.checkOrderIsDone);
                        if (!response.result.IsOpen) {
                            if (response.result.CancelInitiated) {
                                console.log("we canceled");
                                console.log("Uuid: " + response.result.OrderUuid);
                                this.buyStrategyState.tryChangeState(this._strategyCommands.wasCanceledByMe);
                            } else  {
                                console.log("buy is bought");
                                console.log("Uuid: " + response.result.OrderUuid);
                                this.buyStrategyState.tryChangeState(this._strategyCommands.buyIsDone);
                            }
                        } else if (response.result.IsOpen) {
                            //else we already change state in checkOrderIsDone
                            //it is not important what bittrex will answered, we just try closed order if needed
                            if (helpers.convertMillisecondsToSeconds(Date.now() - this._buyInfo.lastStartBuyTime) > this._settings.buyReopenTimeSeconds) {
                                this._tryCloseOrder(this._buyInfo.uuid);
                            }
                        }
                    } else {
                        this.buyStrategyState.tryChangeState(this._strategyCommands.checkOrderIsDone);
                    }
                },
                error => {
                    this.buyStrategyState.tryChangeState(this._strategyCommands.checkOrderIsDone);
                });
        }
    }

    _tryCloseOrder(uuid) {
        this.bittrexController.cancelOrder(uuid).then(
            response => {
                console.log("close result:");
                console.log(response);
            },
            error => {
                console.log("close result:");
                console.log(error);
            });
    }

    _isActualSignalForBuy() {
        return helpers.convertMillisecondsToMinutes(Date.now() - this._startSignalTime) < this._settings.timeForBuyMinutes;
    }

}

module.exports = BuyStrategy;


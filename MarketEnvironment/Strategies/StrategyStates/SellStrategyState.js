/**
 * Created by Никита on 03.01.2018.
 */
//TODO add tests

var constants = require("../../../Constants");
var helpers = require("../../../Helpers");

class SelltrategyState {
    constructor() {
        this._states = constants.strategies.strategySellStates;
        this._commands = constants.strategies.strategySellCommands;
        this.currentState = this._states.readyForSell;
    }

    tryChangeState(command, info) {

        if (command === this._commands.sell) {
            return this._canISell();
        }

        if (command === this._commands.sellAccepted) {
            this._sellOrCancelAccepted();
        }

        if (command === this._commands.sellRejected) {
            this._sellOrCancelRejected();
        }

        if (command === this._commands.checkOrder) {
            return this._canICheckOrder();
        }

        if (command === this._commands.checkOrderIsDone) {
            this._changeCheckOrder();
        }

        if (command === this._commands.wasCanceledByMe) {
            this._orderWasCanceled();
        }

        if (command === this._commands.sellIsDone) {
            this._sellIsDone();
        }

    }

    _canISell() {
        if (this.currentState === this._states.readyForSell) {
            this.currentState = this._states.waitSellOrCancel;
            //console.log(this.currentState);
            return true;
        }
        return false;
    }

    _sellOrCancelAccepted() {
        if (this.currentState === this._states.waitSellOrCancel) {
            this.currentState = this._states.sellingOrCanceling;
            //console.log(this.currentState);
        }
    }

    _sellOrCancelRejected() {
        if (this.currentState === this._states.waitSellOrCancel) {
            this.currentState = this._states.readyForSell;
            //console.log(this.currentState);
        }
    }

    _canICheckOrder() {
        if (this.currentState === this._states.sellingOrCanceling) {
            this.currentState = this._states.checkingOrder;
            //console.log(this.currentState);
            return true;
        }
        return false;
    }

    _changeCheckOrder() {
        if (this.currentState === this._states.checkingOrder) {
            this.currentState = this._states.sellingOrCanceling;
            //console.log(this.currentState);
        }
    }

    _orderWasCanceled() {
        if (this.currentState === this._states.sellingOrCanceling) {
            this.currentState = this._states.readyForSell;
            //console.log(this.currentState);
        }
    }

    _sellIsDone() {
        this.currentState = this._states.sold;
        console.log(this.currentState);
    }

}

module.exports = SelltrategyState;
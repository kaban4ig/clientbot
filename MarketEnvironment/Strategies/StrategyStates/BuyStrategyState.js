/**
 * Created by Никита on 03.01.2018.
 */
//TODO add tests

var constants = require("../../../Constants");
var helpers = require("../../../Helpers");

class BuyStrategyState {
    constructor() {
        this._states = constants.strategies.strategyBuyStates;
        this._commands = constants.strategies.strategyBuyCommands;
        this.currentState = this._states.readyForBuy;
    }

    tryChangeState(command, info) {

        if (command === this._commands.cancelByTime) {
            this._tryCancelByTime();
        }

        if (command === this._commands.buy) {
            return this._canIBuy();
        }

        if (command === this._commands.buyAccepted) {
            this._buyOrCancelAccepted();
        }

        if (command === this._commands.buyRejected) {
            this._buyOrCancelRejected();
        }

        if (command === this._commands.checkOrder) {
            return this._canICheckOrder();
        }

        if (command === this._commands.checkOrderIsDone) {
            this._changeCheckOrder();
        }

        if (command === this._commands.wasCanceledByMe) {
            this._orderWasCanceled();
        }

        if (command === this._commands.buyIsDone) {
            this._buyIsDone();
        }

    }

    _tryCancelByTime() {
        if (this.currentState == this._states.readyForBuy) {
            this.currentState = this._states.buyIsCanceled;
            //console.log(this.currentState);
        }
    }

    _canIBuy() {
        if (this.currentState === this._states.readyForBuy) {
            this.currentState = this._states.waitBuyOrCancel;
            //console.log(this.currentState);
            return true;
        }
        return false;
    }

    _buyOrCancelAccepted() {
        if (this.currentState === this._states.waitBuyOrCancel) {
            this.currentState = this._states.buyingOrCanceling;
            //console.log(this.currentState);
        }
    }

    _buyOrCancelRejected() {
        if (this.currentState === this._states.waitBuyOrCancel) {
            this.currentState = this._states.readyForBuy;
            //console.log(this.currentState);
        }
    }

    _canICheckOrder() {
        if (this.currentState === this._states.buyingOrCanceling) {
            this.currentState = this._states.checkingOrder;
            //console.log(this.currentState);
            return true;
        }
        return false;
    }

    _changeCheckOrder() {
        if (this.currentState === this._states.checkingOrder) {
            this.currentState = this._states.buyingOrCanceling;
            //console.log(this.currentState);
        }
    }

    _orderWasCanceled() {
        if (this.currentState === this._states.buyingOrCanceling) {
            this.currentState = this._states.readyForBuy;
            //console.log(this.currentState);
        }
    }

    _buyIsDone() {
        this.currentState = this._states.bought;
        //console.log(this.currentState);
    }

}

module.exports = BuyStrategyState;
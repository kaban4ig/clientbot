/**
 * Created by Никита on 03.01.2018.
 */
//TODO add tests
var deepcopy = require("deepcopy");
var BuyStrategy = require("./Strategies/BuyStrategy");
var SellStrategy = require("./Strategies/SellStrategy");
var constants = require("../Constants");

class Signal {
    constructor(signalInfo, bittrexController, settings) {
        this._settings = settings || constants.defaultSettings;
        this.isActive = true;

        this._strategyBuyStates = constants.strategies.strategyBuyStates;
        this._strategySellStates = constants.strategies.strategySellStates;

        this.status = {
            buy: true,
            sell: false,
            sellCreating: false,
            sellIsReady: false
        }
        this.strategies = {
            buy: null,
            sells: null
        }

        this.isError = false;
        this.errorMessage = "";

        this.bittrexController = bittrexController;

        this.marketName = signalInfo.marketNames[0];
        this.coinName = this.marketName.substring(this.marketName.indexOf('-')+1);
        this.stopLoss = signalInfo.stop[0];

        //ignore all instead of BTC
        if (!this.marketName.startsWith("BTC")) {
            this.isError = false;
            this.isActive = false;
            this.errorMessage += ";we work only with BTC";
        }

        this.signalObj = deepcopy(signalInfo);
        this.signalObj.marketName = this.marketName;
        this.signalObj.stopLoss = this.stopLoss;
    }

    do() {
        if (this.status.buy && this.strategies.buy === null) {
            this.strategies.buy = new BuyStrategy(this.signalObj, this.bittrexController, this._settings);
        }

        if (this.status.buy) {
            this.strategies.buy.do();
            if (this.strategies.buy.buyStrategyState.currentState === this._strategyBuyStates.bought) {
                this.status.buy = false;
                this.status.sell = true;
            }

            if (this.strategies.buy.buyStrategyState.currentState === this._strategyBuyStates.buyIsCanceled) {
                this.status.buy = false;
                this.status.sell = false;
                this.isActive = false;
            }

        }

        if (this.status.sell && !this.status.sellCreating && !this.status.sellIsReady) {
            this.status.sellCreating = true;
            this.bittrexController.getBalance(this.coinName).then(
                response => {
                    if (response.success === true && response.result.Available) {
                        this.coinCount = response.result.Available;
                        if (this.signalObj.profit.length > this._settings.maxTakeProfitCount) {
                            this.signalObj.profit = this.signalObj.profit.slice(0, this._settings.maxTakeProfitCount);
                        }

                        this.strategies.sells = this._makeNewStrategies();
                        this.status.sellIsReady = true;
                    } else {
                        this.status.sellCreating = false;
                    }
                },
                error => {
                    this.status.sellCreating = false;
                });


        }

        if (this.status.sell && this.status.sellIsReady) {
            this.strategies.sells.forEach(item => {
               item.do();
            });

            if(this.strategies.sells.every(s => s.sellStrategyState.currentState === this._strategySellStates.sold)) {
                this.isActive = false;
                this.status.sell = false;
                //console.log(this.marketName + " done");
            }
        }
    }

    _makeNewStrategies() {
        let result = [];
        let sellCount = this.signalObj.profit.length;
        let oneSellQuantity = parseFloat((this.coinCount / sellCount).toFixed(8));
        let coinRemainder = this.coinCount - oneSellQuantity * (sellCount - 1);

        for (let i = 0; i < sellCount; i++) {
            let marketQuantity;
            if (i === sellCount - 1) {
                marketQuantity = coinRemainder;
            } else {
                marketQuantity = oneSellQuantity;
            }
            let sellSignalObj = {
                marketName: this.signalObj.marketName,
                takeProfit: this.signalObj.profit[i],
                stopLoss: this.signalObj.stopLoss,
                marketQuantity: marketQuantity
            }

            result.push(new SellStrategy(sellSignalObj, this.bittrexController, this._settings, i));

        }
        return result;
    }


}

module.exports = Signal;


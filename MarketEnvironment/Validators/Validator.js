//TODO add tests

class Validator {
    constructor() {
    }

    static checkInfo(infoObj) {
        return Validator._isValidInfo(infoObj);
    }

    static _isValidInfo(infoObj) {
        return Validator._checkIsAndLenght(infoObj.marketNames, 1) &&
            Validator._checkIsAndLenght(infoObj.buy, 1) &&
            Validator._checkIsAndLenght(infoObj.profit, 1) &&
            Validator._checkIsAndLenght(infoObj.stop, 1);
    }

    static _checkIsAndLenght(infoArr, minLenght) {
        let result = false;
        if (infoArr && infoArr.length >= minLenght && !infoArr.some(val => val === null || val === "" || val === undefined)) {
            result = true;
        }

        return result;
    }

}

module.exports = Validator;


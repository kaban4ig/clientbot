class BittrexDataUpdater {


    constructor(bittrexAccessor, marketTickers, marketName, settings) {
        this._bittrexAccessor = bittrexAccessor;
        this.marketTickers = marketTickers;
        this.isActive = true;
        this.marketName = marketName;
        this._settings = settings;
        this._getticker();
    }

    stop() {
        this.marketTickers[this.marketName] = undefined;
        this.isActive = false;
    }

    //TODO add logs
    _getticker() {
        this._bittrexAccessor.getticker( { market : this.marketName }, ( data, err ) => {

            if (this.isActive) {
                //console.log(this.marketName);
                //console.log(data);
                if (data) {
                    if (data.success) {
                        this.marketTickers[this.marketName] = data;
                    } else {
                        this.marketTickers[this.marketName] = undefined;
                    }
                } else if (err) {
                    this.marketTickers[this.marketName] = undefined;
                } else {
                    this.marketTickers[this.marketName] = undefined;
                }

                setTimeout(() => {
                    this._getticker();
                }, this._settings.delay)
                /*if (this._markets.includes(marketName)) {
                    this.getTicker(marketName);
                }*/
            }
        });
    }

}

module.exports = BittrexDataUpdater;
var bittrex = require('node-bittrex-api');
var constants = require('../Constants');
var BittrexDataUpdater = require("./BittrexDataUpdater");

/*const APIKEY = 'e5323488c82d47c19b81b757dfe4152b';
const APISECRET = '283d97b53584452c94b1942d2bf11ed9';*/
const APIKEY = 'a3dd1c2335154dfe8b874977254dd496';
const APISECRET = '4a9bc23c605b47e5afe62c784e300af5';

bittrex.options({
    'apikey' : APIKEY,
    'apisecret' : APISECRET,
    'stream' : false,
    'verbose' : false,
    'cleartext' : false
});

//TODO add tests

class BittrexController {

    constructor() {
        this.isActive = false;
        this.marketTickers = {};
        this.dataUpdaters = [];
    }

    getBalance(coinName) {
        let promise = new Promise((resolve, reject) => {
            bittrex.getbalance({ currency : coinName }, ( data, err ) => {

                if (data) {
                    resolve(data);
                } else {
                    reject(err);
                }
            });
        });

        return promise;
    }

    start() {
        if (!this.isActive) {
            this.isActive = true;
        }
    }

    stop() {
        this.isActive = false;
        this._disableAllMarketsDataUpdater();
    }

    updateMarkets(currentMarkets) {
        if (!currentMarkets) {
            this._disableAllMarketsDataUpdater();
            return;
        }

        var activeDataUpdaters = this.dataUpdaters.map((d) => {
            return d.isActive ? d.marketName: "";
        }).filter(d => {
            return d !== "";
        });

        var newMarkets = currentMarkets.filter(m => !activeDataUpdaters.includes(m));
        var oldMarkets = activeDataUpdaters.filter(m => !currentMarkets.includes(m));

        newMarkets.forEach(market => {
            this.dataUpdaters.push(new BittrexDataUpdater(bittrex, this.marketTickers, market, { delay: 5000 }));
        });

        this.dataUpdaters.forEach(d => {
            if (oldMarkets.includes(d.marketName)) {
                d.stop();
            }
        });

        this._markets = currentMarkets;
        this._removeInactiveDataUpdaters();
    }

    _disableAllMarketsDataUpdater() {
        this.dataUpdaters.forEach(updater => {
            updater.stop();
        });
        this._removeInactiveDataUpdaters();
    }

    _removeInactiveDataUpdaters() {
        this.dataUpdaters = this.dataUpdaters.filter(d => {
            return d.isActive;
        });
    }

    getMarketInfo(marketName) {
        return this.marketTickers[marketName];
    }

    buyLimit(market, quantity, rate) {
        let promise = new Promise(function(resolve, reject) {

            bittrex.buylimit( { market : market, quantity: quantity, rate: rate }, ( data, err ) => {
                if (data) {
                    resolve(data);
                } else {
                    reject(err);
                }
            });

        });

        return promise;
    }

    sellLimit(market, quantity, rate) {
        let promise = new Promise(function(resolve, reject) {

            bittrex.selllimit( { market : market, quantity: quantity, rate: rate }, ( data, err ) => {
                if (data) {
                    resolve(data);
                } else {
                    reject(err);
                }
            });

        });

        return promise;
    }

    getOrder(uuid) {
        let promise = new Promise(function(resolve, reject) {

            bittrex.getorder( { uuid: uuid }, ( data, err ) => {
                if (data) {
                    resolve(data);
                } else {
                    reject(err);
                }
            });
        });

        return promise;
    }

    cancelOrder(uuid) {
        let promise = new Promise(function(resolve, reject) {

            bittrex.cancel( { uuid: uuid }, ( data, err ) => {
                if (data) {
                    resolve(data);
                } else {
                    reject(err);
                }
            });
        });



        return promise;
    }



}

module.exports = BittrexController;




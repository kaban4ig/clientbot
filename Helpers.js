/**
 * Created by Никита on 03.01.2018.
 */
//TODO add tests

function  convertMillisecondsToMinutes(milliseconds) {
    return milliseconds / 1000 / 60;
}

function  convertMillisecondsToSeconds(milliseconds) {
    return milliseconds / 1000;
}

function getNumberFromPercent(number, percent) {
    return number * percent / 100;
}

var helpers = {
    convertMillisecondsToMinutes: convertMillisecondsToMinutes,
    getNumberFromPercent: getNumberFromPercent,
    convertMillisecondsToSeconds: convertMillisecondsToSeconds
}

module.exports = helpers;
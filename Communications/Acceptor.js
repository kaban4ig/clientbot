/**
 * Created by Никита on 03.01.2018.
 */
//TODO add tests

var WebSocketClient = require('websocket').client;

class Acceptor {
    constructor(serverIp) {
        this._serverApi = serverIp;
        this._socket = new WebSocketClient();
        this._subscriber = null;

        this._socket.on('connectFailed', function(error) {
            console.log('Connect Error: ' + error.toString());
        });

        this._socket.on('connect', (connection) => {

            console.log('WebSocket Client Connected');
            connection.on('error', function(error) {
                console.log("Connection Error: " + error.toString());
            });
            connection.on('close', function() {
                console.log('echo-protocol Connection Closed');
            });
            connection.on('message', (message) => {
                if (message.type === 'utf8') {
                    console.log("received message: " + message.utf8Data)
                    this._subscriber(message.utf8Data);
                }
            });

        });
    }

    conntect(subscriber) {
        this._subscriber = subscriber;
        this._socket.connect(this._serverApi, 'echo-protocol');
    }

    isValid() {
        return true;
    }
}

module.exports = Acceptor;


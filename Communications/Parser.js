/**
 * Created by Никита on 03.01.2018.
 */
//TODO add tests

class Parser {
    constructor() {

    }

    getInfoFromJSON(json) {
        let info = JSON.parse(json);
        info.buy = info.buy.map(b => parseFloat(b.replace(',','.'),10));
        info.stop = info.stop.map(s => parseFloat(s.replace(',','.'),10));
        info.profit = info.profit.map(p => parseFloat(p.replace(',','.'),10));

        if (info.settings) {
            info.settings.timeForBuyMinutes = parseFloat(info.settings.timeForBuyMinutes, 10);
            info.settings.amountOfPurchase = parseFloat(info.settings.amountOfPurchase.replace(',','.'), 10);
            info.settings.sellReopenTimeSeconds = parseFloat(info.settings.sellReopenTimeSeconds, 10);
            info.settings.buyReopenTimeSeconds = parseFloat(info.settings.buyReopenTimeSeconds, 10);
            info.settings.addBuy2Percent = parseFloat(info.settings.addBuy2Percent, 10);
            info.settings.maxTakeProfitCount = parseFloat(info.settings.maxTakeProfitCount, 10);
        }

        return info;
    }

}

module.exports = Parser;

